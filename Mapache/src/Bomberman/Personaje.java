package Bomberman;

public interface Personaje {
	
	public abstract void move(); 
	
	public abstract void action();
	
	public abstract void die();

}
